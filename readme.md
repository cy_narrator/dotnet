# Project files for Dotnet Class
## What to install
### Dotnet Framework
You need Dotnet 7.0 and higher to run the files in here.
#### Using Microsoft Visual Studio
If you have a modern computer with high enough resources and you dont mind sparing some storage space in your computer, then Visual Studio is the best option for you. Visual Studio makes developing .NET programs slightly easier as it does most of the setup for you. Please follow the guide on [Visual Studio](visual_studio.md) to learn more

#### Using Dotnet SDK
While it is a good idea to stick to Visual Studio for better experience with Dotnet, one should not forget that Visual Studio can be very resource intensive, especially on a lower end computer. For that reason, you can install only Dotnet SDK and develop Dotnet programs in your computer. This is the only option if you are on Linux systems. Please follow the guide on [Dotnet SDK](dotnet_sdk.md) to learn more.

I prefer Dotnet SDK for these projects because it is lightweight.

### Relational Database Management System
For this you can pick up any of the RDBMS out there. There are notes on how to setup Postgresql, MariaDB(MySQL) and MS SQL Server. It is more advised if you choose MS SQL Server because C# and Dotnet tends to be paired heavily with MS SQL Server. Yet do recognize that many database management systems are supported for Dotnet and many operate the same way.

## How to download files from here
### Using Git command line
1: Clone this project to your computer using the following command
```
git clone https://gitlab.com/cy_narrator/dotnet
```

2: It should be put inside a folder named `dotnet` in the current directory

Once you get this cloned, you can very easily update to the latest code files by running the following command
Make sure you are inside the `dotnet` directory
```
git pull origin main
```

### Without using Git
1: Find the white download button on the right side of the screen if using Desktop/Laptop

2: Click on the zip file to download
