﻿/* This code follows horrible coding practises but is only intended to make things very simple */

/* First of all, you need to install MySqlConnector in your project For that, follow the steps,
 1: If you are using Visual Studio
    *  0: Make sure your MariaDB or MySQL server works and prepare it with a new user and database if needed
    *  1: Open Visual studio and go to Tools(in the menu bar) > NuGet Package Manager > Package Manager Console
    *  2: You will get a little console with the prompt as `PM>`
    *  3: Type the following in the prompt in between ``` ``` exactly
    *      ```
    *          Install-Package MySqlConnector
    *      ```
    *  4: Wait for it to install

 2: If you are using dotnet sdk command line and dont have visual studio, follow the steps,
    *  0: Make sure your MariaDB or MySQL server works and prepare it with a new user and database if needed
    *  1: Make sure you are inside the project directory in your command line(where Program.cs is)
    *  2: Run the following command exactly
        ```
               dotnet add package MySqlConnector
        ```
    *  3: Wait for it to install before you run the project
*/

/*
    *  Important thing to note is that MariaDB and MySQL are compatible with each other.
    *  This means that you can either run this in MySQL server or MariaDB server, it will be the same.
    *  This will also work with XAMPP if you have it installed, just create appropriate user and database.
    *  The default mysql/mariadb database is 'root' so you can just change db_user to "root" if you want.
    *  XAMPP also switched to MariaDB for quiet some time now.
*/

/* 
    *  To create a new user in MySQL as what is in this note, use the following in the mysql command line client
    *  Note that you run these command as the database super user which is 'root' by default
        [Dont confuse with your local root user in Linux]
        ```
            CREATE USER 'dotnetuser'@'localhost' IDENTIFIED BY 'password';
            CREATE DATABASE dotnetprojects;
            GRANT ALL PRIVILEGES ON dotnetprojects.* TO 'dotnetuser'@'localhost' IDENTIFIED BY 'password';
            FLUSH PRIVILEGES;
        ```
*/

using System;
using MySqlConnector; /* This comes from installing MySqlConnector using NuGet above */
namespace MariaDB
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /* 
              Before we start, keep in mind how the table was created in mysql;

                        create table users (
                            id SERIAL,
                            username varchar(60) not null unique,
                            password varchar(60) not null,
                            primary key(id)
                        );
            */

            /* Define connection parameters here. This will be used inside connection string */
            string db_host = "localhost";
            int db_port = 3306;
            string db_user = "dotnetuser";
            string db_password = "password";
            string mariadb_database_name = "dotnetprojects";
            string ssl_mode = "none";

            /* First have your connection string, that password has a ' ' surrounding the password characters */

            string connection_string = $"server={db_host};port={db_port};database={mariadb_database_name};user={db_user};password='{db_password}';SslMode = {ssl_mode};";
            
            /* The above connection string was copied from the documentation and all the projects using mysql use the same connection string, except you need to change necessary values in each case */


            /* Next, create the connection object for the database with type MySqlConnection and pass in the connection string we had earlier */
            MySqlConnection connection = new MySqlConnection(connection_string);

            /* Open the connection */
            connection.Open();

            /* Now we write database queries, first, we write the simple insert query */
            string simple_insert1 = "INSERT INTO users(username, password) VALUES('a','b')";
            string simple_insert2 = "INSERT INTO users(username,password) VALUES ('c', 'd')";

            MySqlCommand simple_cmd1 = new MySqlCommand(simple_insert1, connection);
            MySqlCommand simple_cmd2 = new MySqlCommand(simple_insert2, connection);

            try
            {
                int result = simple_cmd1.ExecuteNonQuery();
                int result2 = simple_cmd2.ExecuteNonQuery();
                Console.WriteLine($"Number of rows affected: {result + result2}");
            }
            catch (MySqlConnector.MySqlException mysql_exception)
            {
                /* This catches the exception thrown by the database server itself.
                   What if you tried to insert same value twice but the database server had a unique property in that field */
                Console.WriteLine(mysql_exception.Message);
                Console.WriteLine(mysql_exception.StackTrace);
            }

            /* So we will look at a better insert method now */
            string better_insert = "INSERT INTO users(username, password) VALUES(@enter_username,@enter_password)";
            string better_insert2 = "INSERT INTO users(username, password) VALUES(@enter_username, @enter_password)";

            MySqlCommand better_cmd = new MySqlCommand(better_insert, connection);
            MySqlCommand better_cmd2 = new MySqlCommand(better_insert2, connection);

            /* Now we need to replace those @ values with the actual data to be entered in the database */
            /* Note that this is not something possible in any regular string object as you might think but is special method in the MySqlCommand object */
            better_cmd.Parameters.AddWithValue("enter_username", "e");
            better_cmd.Parameters.AddWithValue("enter_password", "f");
            better_cmd2.Parameters.AddWithValue("enter_username", "g");
            better_cmd2.Parameters.AddWithValue("enter_password", "h");

            /* Now we execute */
            try
            {
                int result = better_cmd.ExecuteNonQuery();
                int result2 = better_cmd2.ExecuteNonQuery();
                Console.WriteLine($"Number of rows affected: {result + result2}");
            }
            catch (MySqlConnector.MySqlException mysql_exception)
            {
                /* This catches the exception thrown by the database server itself.
                   What if you tried to insert same value twice but the database server had a unique property in that field */
                Console.WriteLine(mysql_exception.Message);
                Console.WriteLine(mysql_exception.StackTrace);
            }

            /* ------------------------------------------ Updating table data ------------------------------------------ */

            /* Update is almost similar to insert. You just use the update query in place of insert and thats it */
            /* This can be done in both simple and better ways but you almost always want to do it in better way */

            string update_query = "UPDATE users SET username = @newusername, password = @newpassword WHERE username = @old_username";
            MySqlCommand update_cmd = new MySqlCommand(update_query, connection);

            /* Just like above we replace those in @ with what the actual value shall be */
            update_cmd.Parameters.AddWithValue("newusername", "updated username a");
            update_cmd.Parameters.AddWithValue("newpassword", "updated password b");
            update_cmd.Parameters.AddWithValue("old_username", "a");
            /* We execute */
            try
            {
                int reader = update_cmd.ExecuteNonQuery();
                Console.WriteLine($"Number of rows affected = {reader}");
            }
            catch (MySqlConnector.MySqlException mysql_exception)
            {
                /* This catches the exception thrown by the database server itself.
                   What if you tried to update row that does not exist */
                Console.WriteLine(mysql_exception.Message);
                Console.WriteLine(mysql_exception.StackTrace);
            }



            /* ------------------------------------------ Deleting table data ------------------------------------------ */

            /* Exactly as above but instead you use the delete query */
            string delete_query = "DELETE FROM users WHERE username = @username";

            MySqlCommand delete_cmd = new MySqlCommand(delete_query, connection);
            delete_cmd.Parameters.AddWithValue("username", "e");
            /* We execute */
            try
            {
                int reader = delete_cmd.ExecuteNonQuery();
                Console.WriteLine($"Number of rows affected = {reader}");
            }
            catch (MySqlConnector.MySqlException mysql_exception)
            {
                /* This catches the exception thrown by the database server itself.
                   What if you tried to update row that does not exist */
                Console.WriteLine(mysql_exception.Message);
                Console.WriteLine(mysql_exception.StackTrace);
            }

            /* ------------------------------------------ Selecting/Reading table data ------------------------------------------ */

            /* First, create a query string as above */
            string select_string = "SELECT * FROM users";

            /* Create cmd */
            MySqlCommand insert_cmd = new MySqlCommand(select_string, connection);
            /* Because there is no need to insert anything into the command, we can just use query string normally */

            /* Now we execute */
            try
            {
                /* Previously, the only data we got was how many rows were affected and we did not care about anything else.
                   This time however, we are running select query and that means we expect to receive data from the database.
                   So we use use the ExecuteReader method */
                MySqlDataReader received_data = insert_cmd.ExecuteReader();
                while (received_data.Read()) /* While there are more rows to read from the database */
                {
                    Console.WriteLine(received_data.GetInt32(0)); /* Column number 0 (id): Convert to int32(32 bit/4 byte int) type after receiving */
                    Console.WriteLine(received_data.GetString(1)); /* Column number 1 (username): Convert to string type after receiving */
                    Console.WriteLine(received_data.GetString(2)); /* Column number 2 (password): Convert to string type after receiving */
                }
            }
            catch (MySqlConnector.MySqlException mysql_exception)
            {
                Console.WriteLine(mysql_exception.Message);
                Console.WriteLine(mysql_exception.StackTrace);
            }

            /* The difference between ExecuteReader and ExecuteNonQuery should be obvious by now,
               We use ExecuteNonQuery if we just want to send data to the database and at most we care the number of rows affected
               We use ExecuteReader if we want to actually receive the data rows from the database in our program that we can loop over and operate on */

            /* Do not forget to close after you are done */
            connection.Close();
        }

    }
}