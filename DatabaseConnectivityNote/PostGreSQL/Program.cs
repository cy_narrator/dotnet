﻿/* This code follows horrible coding practises but is only intended to make things very simple */

/* First of all, you need to install Npgsql in your project For that, follow the steps,
 1: If you are using Visual Studio
    *  0: Make sure your postgresql server works and prepare it with a new user and database if needed
    *  1: Open Visual studio and go to Tools(in the menu bar) > NuGet Package Manager > Package Manager Console
    *  2: You will get a little console with the prompt as `PM>`
    *  3: Type the following in the prompt in between ``` ``` exactly
    *      ```
    *          Install-Package Npgsql
    *      ```
    *  4: Wait for it to install

 2: If you are using dotnet sdk command line and dont have visual studio, follow the steps,
    *  0: Make sure your postgresql server works and prepare it with a new user and database if needed
    *  1: Make sure you are inside the project directory in your command line(where Program.cs is)
    *  2: Run the following command exactly
        ```
               dotnet add package Npgsql
        ```
    *  3: Wait for it to install before you run the project
*/

/*
    *  To create a new user in Postgresql as what is in this note, use the following in the psql client
    *  Note that you run these command as the super user which is 'postgres' by default
        ```
            CREATE ROLE dotnetuser WITH PASSWORD 'password' LOGIN;
            CREATE DATABASE dotnetprojects WITH OWNER dotnetuser;
        ```
*/


using System;
using Npgsql; /* This comes from installing Npgsql using NuGet above */

namespace PostGreSQL
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*
              Before we start, keep in mind how the table was created in postgresql;

                        create table users (
                            id SERIAL,
                            username varchar(60) not null unique,
                            password varchar(60) not null,
                            primary key(id)
                        );
            */
            /* Define connection parameters here. This will be used inside connection string */
            string db_host = "localhost";
            int db_port = 5432;
            string db_user = "dotnetuser";
            string db_password = "password";
            string postgres_database_name = "dotnetprojects";

            /* First have your connection string, that password has a ' ' surrounding the password characters */

            string connection_string = $"Host={db_host}:{db_port};Username={db_user};Password='{db_password}';Database={postgres_database_name};";

            /* The above connection string was copied from the documentation and all the projects using postgresql use the same connection string, except you need to change necessary values in each case */

            /* Next, create the connection object for the database with type NpgsqlConnection and pass in the connection string we had earlier */
            NpgsqlConnection connection = new NpgsqlConnection(connection_string);

            /* Open the connection */
            connection.Open();

            /* Now we write database queries, first, we write the simple insert query */
            string simple_insert1 = "INSERT INTO users(username, password) VALUES('a','b')";
            string simple_insert2 = "INSERT INTO users(username, password) VALUES('c', 'd')";

            /* Now, we create a command object that stores the query and connection object */
            NpgsqlCommand simple_cmd1 = new NpgsqlCommand(simple_insert1, connection);
            NpgsqlCommand simple_cmd2 = new NpgsqlCommand(simple_insert2, connection);

            /* Finally we execute the cmd object */
            try
            {
                int reader = simple_cmd1.ExecuteNonQuery();
                int reader2 = simple_cmd2.ExecuteNonQuery();
                Console.WriteLine($"Number of rows affected = {reader + reader2}");
            }
            catch (Npgsql.PostgresException postgresql_exception)
            {
                /* This catches the exception thrown by the database server itself.
                   What if you tried to insert same value twice but the database server had a unique property in that field */
                Console.WriteLine(postgresql_exception.Message);
                Console.WriteLine(postgresql_exception.InternalQuery);
            }
            /* The data should have been entered into the database if you check it */
            /* Though the simple insert method described above works, your application can be vulnerable to SQL injection attacks and other problems */
            /* So to mitigate the risk, we use a more complicated but better method of entering data */

            /* So we will look at a better insert method now */
            string better_insert = "INSERT INTO users(username, password) VALUES(@enter_username,@enter_password)";
            string better_insert2 = "INSERT INTO users(username, password) VALUES(@enter_username, @enter_password)";

            /* As before, create a command object */
            NpgsqlCommand better_cmd = new NpgsqlCommand(better_insert, connection);
            NpgsqlCommand better_cmd2 = new NpgsqlCommand(better_insert2, connection);

            /* Now we need to replace those @ values with the actual data to be entered in the database */
            /* Note that this is not something possible in any regular string object as you might think but is special method in the NpgsqlCommand object */
            better_cmd.Parameters.AddWithValue("enter_username", "e");
            better_cmd.Parameters.AddWithValue("enter_password", "f");
            better_cmd2.Parameters.AddWithValue("enter_username", "g");
            better_cmd2.Parameters.AddWithValue("enter_password", "h");

            /* We execute */
            try
            {
                int reader = better_cmd.ExecuteNonQuery();
                int reader2 = better_cmd2.ExecuteNonQuery();
                Console.WriteLine($"Number of rows affected = {reader + reader2}");
            }
            catch (Npgsql.PostgresException postgresql_exception)
            {
                /* This catches the exception thrown by the database server itself.
                   What if you tried to insert same value twice but the database server had a unique property in that field */
                Console.WriteLine(postgresql_exception.Message);
                Console.WriteLine(postgresql_exception.InternalQuery);
            }



            /* ------------------------------------------ Updating table data ------------------------------------------ */

            /* Update is almost similar to insert. You just use the update query in place of insert and thats it */
            /* This can be done in both simple and better ways but you almost always want to do it in better way */

            string update_query = "UPDATE users SET username = @newusername, password = @newpassword WHERE username = @old_username";
            NpgsqlCommand update_cmd = new NpgsqlCommand(update_query, connection);

            /* Just like above we replace those in @ with what the actual value shall be */
            update_cmd.Parameters.AddWithValue("newusername", "updated username a");
            update_cmd.Parameters.AddWithValue("newpassword", "updated password b");
            update_cmd.Parameters.AddWithValue("old_username", "a");
            /* We execute */
            try
            {
                int reader = update_cmd.ExecuteNonQuery();
                Console.WriteLine($"Number of rows affected = {reader}");
            }
            catch (Npgsql.PostgresException postgresql_exception)
            {
                /* This catches the exception thrown by the database server itself.
                   What if you tried to update row that does not exist */
                Console.WriteLine(postgresql_exception.Message);
                Console.WriteLine(postgresql_exception.InternalQuery);
            }



            /* ------------------------------------------ Deleting table data ------------------------------------------ */

            /* Exactly as above but instead you use the delete query */
            string delete_query = "DELETE FROM users WHERE username = @username";

            NpgsqlCommand delete_cmd = new NpgsqlCommand(delete_query, connection);
            delete_cmd.Parameters.AddWithValue("username", "e");
            /* We execute */
            try
            {
                int reader = delete_cmd.ExecuteNonQuery();
                Console.WriteLine($"Number of rows affected = {reader}");
            }
            catch (Npgsql.PostgresException postgresql_exception)
            {
                /* This catches the exception thrown by the database server itself.
                   What if you tried to update row that does not exist */
                Console.WriteLine(postgresql_exception.Message);
                Console.WriteLine(postgresql_exception.InternalQuery);
            }


            /* ------------------------------------------ Selecting/Reading table data ------------------------------------------ */

            /* First, create a query string as above */
            string select_string = "SELECT * FROM users";

            /* Create cmd */
            NpgsqlCommand insert_cmd = new NpgsqlCommand(select_string, connection);
            /* Because there is no need to insert anything into the command, we can just use query string normally */

            /* Now we execute */
            try
            {
                /* Previously, the only data we got was how many rows were affected and we did not care about anything else.
                   This time however, we are running select query and that means we expect to receive data from the database.
                   So we use use the ExecuteReader method */
                NpgsqlDataReader received_data = insert_cmd.ExecuteReader();
                while (received_data.Read()) /* While there are more rows to read from the database */
                {
                    Console.WriteLine(received_data.GetInt32(0)); /* Column number 0 (id): Convert to int32(32 bit/4 byte int) type after receiving */
                    Console.WriteLine(received_data.GetString(1)); /* Column number 1 (username): Convert to string type after receiving */
                    Console.WriteLine(received_data.GetString(2)); /* Column number 2 (password): Convert to string type after receiving */
                }
            }
            catch (Npgsql.PostgresException postgresql_exception)
            {
                Console.WriteLine(postgresql_exception.Message);
                Console.WriteLine(postgresql_exception.InternalQuery);
            }

            /* The difference between ExecuteReader and ExecuteNonQuery should be obvious by now,
               We use ExecuteNonQuery if we just want to send data to the database and at most we care the number of rows affected
               We use ExecuteReader if we want to actually receive the data rows from the database in our program that we can loop over and operate on */

            /* Do not forget to close after you are done */
            connection.Close();
        }

    }
}
