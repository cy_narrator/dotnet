# Instructions for using Dotnet SDK
## What to install
-  **Required** Dotnet SDK
 -  **Required** A Text Editor

## Installation Instruction
### Windows or Mac
- Go to [Dotnet Download Page](https://dotnet.microsoft.com/en-us/download)
- Download the latest LTS version
- Install it
[If you have a CMD window open, you will have to close and reopen it after installing dotnet sdk to have dotnet command working]
### Linux
You should have dotnet sdk in your distribution's package manager, you can install it from there

## Text editor
You can use any text editor available in your platform from VS Code to Pulsar-dev or even Vim or Emacs

## Creating new Projects with dotnet sdk
### Listing all the available project types
Run the command
```
dotnet new list
```
Which will list all the names of the template project available. Note the `short name` of the project you want to create. You will use that name to tell dotnet what project you wish to create

### Creating a new Console application
You can create a new console app by the following command
```
dotnet new console -o MyConsoleApp
```
[Note that the word `console` was listed in the short name for Console Application where MyConsoleApp is the name for the console application]

To create a new Dotnet console application without using `Top Level Statement` one can do
```
dotnet new console -o MyConsoleApp --use-program-main
```
[Refer to the documentation on the available flags for different Project templates](https://learn.microsoft.com/en-us/dotnet/core/tools/dotnet-new-sdk-templates#console)

## Run the project
In order for you to run the project, you need to be in the same directory where your `.csproj` file exists
### Just Build
To only build the project into an executable and/or library, run the following
```
dotnet build
```
### Build and run on the same command
To run the project with a single command, run the following
```
dotnet run
```
### Delete precompiled binaries
If for some reason you need to clean all the binaries, run the following command
```
dotnet clean
```
### Build and run with 'live server'
This is useful mostly in ASP .NET where you do not want to keep restarting the server. The server will automatically restart when you make some changes to the file, even if there were syntax errors. Run the following
```
dotnet watch
```
## Installing NuGet packages
You can install NuGet packages using the `dotnet add package` command when you are into your project directory(where that `.csproj` file is located. Such as,
```
dotnet add package <Package_Name> --version <Version_Number>
```
[You can omit `--version <Version_Number>` Number if you want]
