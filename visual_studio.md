
# Instructions for using Visual Studio
You can use Visual Studio Community Edition
## What to install
Open up the Visual Studio Installer and select to install the following
 -  **Required** Web & Cloud / ASP .NET and web development
 -  **Required** Desktop & Mobile/ .NET development desktop
 -  **Recommended** Other Toolsets/ Data storage and processing
 -  **Optional suggested** Other Toolsets/Visual Studio Extension Development
 -  **Optional for C++** Desktop & Mobile/ Desktop development with C++

## Disable AI in Visual Studio
- Open up Visual Studio Installer
- Select `Modify`
- Find the section named `Installation Details` on the right section of the program
- Under `Optional Section` untick `IntelliCode`
- Click on `Modify` and wait till things are ready

## Run project in Visual Studio
### If there is a .sln file present
If there is a .sln file present, you can double click on that file and everything will be loaded right inside Visual Studio.

### If there is no .sln file present
If a project was created using dotnet sdk, it will not come with .sln file by default. In such a case, you can Open Visual Studio and select `Open Folder` and navigate to the folder where the code is located. Note that the Project folder should have `.csproj` file present

### Install NuGet Packages
Nuget Package Manager allows you to install external libraries into your project. To install NuGet packages in Visual Studio, follow the following steps.
    *   Make sure your MariaDB or MySQL server works and prepare it with a new user and database if needed
    *   Open Visual studio and go to Tools(in the menu bar) > NuGet Package Manager > Package Manager Console
    *  You will get a little console with the prompt as `PM>`
    *  Type the following in the prompt exactly
    ```
        Install-Package <Package_Name>
    ```

    *  Wait for it to install
