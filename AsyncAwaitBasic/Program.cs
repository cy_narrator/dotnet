namespace AsyncAwaitBasics;
using System;
internal class MainClass
{
    private static int Synccount_to_100()
    {
        int i;
        for(i = 0; i < 99999; i++)
        {
            Console.WriteLine($"Running Syncronously: {i}");
        }
        return i;
    }
    private static int Asynccount_to_100()
    {
        int i;
        for(i = 0; i < 99999; i++)
        {
            Console.WriteLine($"Running Asyncronously: {i}");
        }
        return i;
    }
    async private static Task PerformTaskAsync()
    {
        await Task.Run(() => Asynccount_to_100());
    }
    private static void PerformTaskSync()
    {
        Synccount_to_100();
    }
    public static void Main(string[] args)
    {
        PerformTaskAsync();
        PerformTaskSync();
        Console.WriteLine("Hello");
    }
}
