using System;
using System.Data;
namespace ADO_Without_DB;
internal class MainClass
{
    static void Main(string[] args)
    {
        /* First we create a table to put our data */
        DataTable table = new DataTable("Student"); // The name of our table

        /* Now we begin adding columns to the table. We use DataColumn objects */
        DataColumn id = new DataColumn("ID"); // ID being the name of the column while ID being the object representing the ID column
        id.DataType = typeof(int);
        id.Unique = true;
        id.AutoIncrement = true;
        id.AllowDBNull = false;
        id.Caption = "Student ID";

        /* Create another column */
        DataColumn name = new DataColumn("Name");
        name.MaxLength = 50;
        name.AllowDBNull = false;

        /* Finally add the email column */
        DataColumn email = new DataColumn("Email");

        /* Now start adding all those above columns into our DataTable */
        table.Columns.Add(id);
        table.Columns.Add(name);
        table.Columns.Add(email);

        /* Also add the primary key into the table */
        table.PrimaryKey = new DataColumn[] {id};
        /* This is an array of DataColumn objects. Which means we could have added one or many primary keys */

        /* At this point our table is ready, now we just need to add Data to it */
        /* We add data to the table by using DataRow object */

        DataRow row1 = table.NewRow();
        row1["name"] = "Std1";
        row1["email"] = "std1@email.com";
        table.Rows.Add(row1);

        /* But there is another way of adding all this in one line */
        table.Rows.Add(null, "Std2", "std2@email.com");
        /* Remember that id has a Autoincrement set so we dont need to touch it */
        /* However we still have to pass null because Constructor will pass it in order */
        string[] names = {"Std3", "Std4", "Std5", "Std6", "std7"};
        string[] emails = {"email@gmail.com", "email2@gmail.com","email3@gmail.com", "email4@gmail.com", "email5@gmail.com"};

        for(int i = 0; i < names.Length; i++)
        {
            table.Rows.Add(null, names[i], emails[i]);
        }

        /* Deleting a row where name is Std3 */
        /* Copied from Stack Overflow: https://stackoverflow.com/questions/5648339/deleting-specific-rows-from-datatable */
        /* There are unnecessary conversion to string to get rid of warning CS0252: Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string' */
        for(int i = table.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = table.Rows[i];
            if(Convert.ToString(dr["name"]) == "Std3")
            {
                dr.Delete();
            }
        }

        foreach(DataRow table_row in table.Rows)
        {
            if(Convert.ToString(table_row["name"]) == "Std4")
            {
                table_row["name"] = "Updated Std4";
            }
        }


        /* Looping through DataTable and displaying result */
        foreach(DataRow row in table.Rows)
        {
            Console.WriteLine($"id={row["id"]} name={row["name"]} email={row["email"]}");
        }


    }
}
