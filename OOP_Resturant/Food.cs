namespace Resturant;
public class Food
{
    int price;
    string name;
    string[] available_foods = {"Pizza", "Burger", "Chowmin"};

    public string getName()
    {
        return this.name;
    }

    public int getPrice()
    {
        return this.price;
    }

    public Food(string name)
    {
        this.name = name;
        switch(name)
        {
            case "Pizza":
                this.price = 100;
                break;
            case "Burger":
                this.price = 200;
                break;
            case "Chowmin":
                this.price = 300;
                break;
            default:
                this.price = 500;
                break;
        }
    }
}
