namespace Resturant;
class Employee
{
    private static int number_of_employees = 0;
    private string name;
    private string position;

    public string getName()
    {
        return this.name;
    }

    protected static void add_employee_count()
    {
        if(Employee.number_of_employees == 0)
        {
            Employee.number_of_employees = 1;
        }
        else
        {
            Employee.number_of_employees += 1;
        }
    }

    protected static int getEmployeeCount()
    {
        return Employee.number_of_employees;
    }

    protected Employee(string name, string position)
    {
        this.name = name;
        this.position = position;
    }
}
