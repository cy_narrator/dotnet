namespace Resturant;
class Cook: Employee
{
    public Cook(string name) : base(name, "Cook")
    {
        add_employee_count();
    }
    public Food mkFood(Food food)
    {
        System.Console.WriteLine($"{food.getName()} is being cooked by {this.getName()}");
        return food;
    }
}
