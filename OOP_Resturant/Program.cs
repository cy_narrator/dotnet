namespace Resturant;
using System;
public class MainClass
{
    public static void Main(string[] args)
    {
        Waiter waiter = new Waiter("WaiterMan");
        bool continue_ordering = true;
        int price = 0;
        while(continue_ordering)
        {
            string food_name;
            do
            {
                Console.WriteLine("Enter the food you want");
                food_name = Console.ReadLine();
            } while(food_name == null);
            if(food_name == "stop")
            {
                continue_ordering = false;
                break;
            }
            else
            {
                Food food = new Food(food_name);
                price += waiter.take_order(food);
            }
        }
        Console.WriteLine($"Your total bill is {price}");
    }
}
