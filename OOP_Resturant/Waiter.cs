namespace Resturant;
class Waiter: Employee
{
    public Waiter(string name): base(name, "Waiter")
    {
        add_employee_count();
    }

    public int take_order(Food food)
    {
        Cook cook = new Cook("CookMan");
        food = cook.mkFood(food);
        Console.WriteLine($"Here is your {food.getName()} please enjoy");
        return food.getPrice();
    }
}
